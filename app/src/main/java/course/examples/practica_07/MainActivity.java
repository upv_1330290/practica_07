package course.examples.practica_07;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void call(View view) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            EditText e = (EditText)findViewById(R.id.txtNumero);
            String a = e.getText().toString();

            callIntent.setData(Uri.parse("tel:"+a));

            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Call failed", activityException);
        }
    }

    public void enviarMensaje(View view){
        try {
            EditText e = (EditText)findViewById(R.id.txtNumero);
            String a = e.getText().toString();

            String message = ((EditText)findViewById(R.id.txtMensaje)).getText().toString();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + a));
            intent.putExtra("sms_body", message);
            //intent.setType("vnd.android-dir/mms-sms");
            startActivity(intent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("dialing-example", "Mensaje Fallado", activityException);
        }
    }

}
